# Trabalho Prático 1

## Integrantes
- Diego de Jesus Ferreira 
- Mateus Jorge
- Sávio Reis Castellar
- Vitor Diocleciano

## Vídeo
- Disponível em: https://youtu.be/hlsUSHuwnfI
## Documentação do código
A proposta do código contido neste repositório é executar a leitura de alguns arquivos no formato .txt e aplicar métodos de ordenação, filtragem e seleção através dos conceitos da Prgramação Orientada a Objetos.<br>

### Estrutura do código:
``` bash
    |--- files
    |    |-- 1.txt
    |    |-- 2.txt
    |    |   .
    |    |   .
    |    |   .
    |    |-- 16.txt
    |--- src
         |-- Headers
         |   |-- audiobook.h
         |   |-- eletronico.h
         |   |-- impresso.h
         |   |-- livro.h
         |-- Source
         |   |-- audiobook.cpp
         |   |-- eletronico.cpp
         |   |-- impresso.cpp
         |   |-- livro.cpp
         |-- main.cpp
```

As classes seguem uma estrutura de hierarquia para permitir o compartilhamento de atributos e métodos. <br>
### Estrutura das classes: <br>
```Ruby
                        Livros
                          |
             _____________|_____________
            |             |             |
            |             |             |
        Impresso     Eletronico     AudioBook
```
## Compilação
Para compilar o código é necessário executar os arquivos a partir do diretório ```src```.
``` Ruby
> cd src
```
Utilizando o compilador ```g++```, execute o comando a seguir para fazer com que os arquivos ```.cpp``` que contém as classes sejam reconhecidos.
``` Ruby
> g++ Source/livro.cpp Source/audiobook.cpp Source/impresso.cpp Source/eletronico.cpp main.cpp -o test
```
Uma vez executado o comando acima, um arquivo denominado ```test.exe``` é gerado no diretório atual.

## Execução
O comando a seguir executa o arquivo que acabou de ser gerado:
``` Ruby
> .\test.exe
```
