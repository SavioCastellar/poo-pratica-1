#ifndef ELETRONICO_H_INCLUDED
#define ELETRONICO_H_INCLUDED

#include "livro.h"

#include <string>
#include <vector>
using std::string;
using namespace std;

class Eletronico : public Livro
{
    string url;
    string formatoArquivo;

public:
    Eletronico(
        vector<string>,
        vector<string>,
        vector<string>,
        string = "",
        int = 0,
        string = "",
        string = "",
        string = ""
    );
    Eletronico();

    void setUrl(string);
    string getUrl() const;

    void setFormatoArquivo(string);
    string getFormatoArquivo() const;

    virtual void print(ostream&) const;

    friend ostream& operator<<(ostream& out, const Eletronico&);
    virtual void setCaracteristicaEspecifica();
};
#endif