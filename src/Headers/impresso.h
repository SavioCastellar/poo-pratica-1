#ifndef IMPRESSO_H_INCLUDED
#define IMPRESSO_H_INCLUDED

#include "livro.h"

#include <string>
#include <vector>
using std::string;
using namespace std;

class Impresso : public Livro
{
    vector<string> livrarias;
    int colunas;

public:
    Impresso(
        vector<string>,
        vector<string>,
        vector<string>,
        vector<string>,
        string = "",
        int = 0,
        string = "",
        int = 0
    );
    Impresso();

    void setLivrarias(vector<string>);
    vector<string> getLivrarias() const;

    void setColunas(int);
    int getColunas() const;

    friend ostream& operator<<(ostream& out, const Impresso&);

    virtual void setCaracteristicaEspecifica();

    virtual void print(ostream&) const;
};

#endif