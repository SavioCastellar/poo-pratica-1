#include <iostream>
#include <list>
#include <iomanip>

#include "../Headers/livro.h"
#include "../Headers/audiobook.h"

AudioBook::AudioBook() {}

AudioBook::AudioBook(
    std::vector<string> escritores,
    std::vector<string> capitulos,
    std::vector<string> keywords,
    std::string titulo,
    int anoPublicacao,
    std::string idiomaOriginal,
    float duracao,
    std::string formatoAudio) : 
    Livro(escritores, capitulos, keywords, titulo, anoPublicacao, idiomaOriginal),
    duracao (duracao),
    formatoAudio (formatoAudio) {}

void AudioBook::setDuracao(float duracao){
    this->duracao = duracao;
}

float AudioBook::getDuracao() const{
    return duracao;
}

void AudioBook::setFormatoAudio(std::string formatoAudio){
    this->formatoAudio = formatoAudio;
}

std::string AudioBook::getFormatoAudio() const{
    return formatoAudio;
}

void AudioBook::setCaracteristicaEspecifica(){}

void AudioBook::print(std::ostream&out) const{
    Livro::print(out);
    out << fixed << setprecision(2) << duracao;
}

std::ostream& operator<<(std::ostream& out, const AudioBook& audiobook ) {
    audiobook.print(out);
    return out;   
} 