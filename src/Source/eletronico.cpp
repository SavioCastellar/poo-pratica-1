#include <iostream>

#include "../Headers/eletronico.h"

Eletronico::Eletronico(
    std::vector<string> escritores,
    std::vector<string> capitulos,
    std::vector<string> keywords,
    std::string titulo,
    int anoPublicacao,
    std::string idiomaOriginal,
    std::string url,
    std::string formatoArquivo) :
    Livro(escritores, capitulos, keywords, titulo, anoPublicacao, idiomaOriginal),
    url (url),
    formatoArquivo (formatoArquivo) {}

Eletronico::Eletronico(){}

void Eletronico::setUrl(std::string url){
    this->url = url;
}

std::string Eletronico::getUrl() const{
    return url;
}

void Eletronico::setFormatoArquivo(std::string formatoArquivo){
    this->formatoArquivo = formatoArquivo;
}

std::string Eletronico::getFormatoArquivo() const{
    return formatoArquivo;
}

void Eletronico::setCaracteristicaEspecifica(){}

void Eletronico::print(std::ostream&out) const{
    Livro::print(out);
    out << formatoArquivo;
}

std::ostream& operator<<(std::ostream& out, const Eletronico& eletronico ) {
    eletronico.print(out);
    return out;   
} 