#include <iostream>

#include "../Headers/impresso.h"

Impresso::Impresso(
    std::vector<string> escritores,
    std::vector<string> capitulos,
    std::vector<string> keywords,
    std::vector<string> livrarias,
    std::string titulo,
    int anoPublicacao,
    std::string idiomaOriginal,
    int colunas): 
    Livro(escritores, capitulos, keywords, titulo, anoPublicacao, idiomaOriginal),
    livrarias (livrarias),
    colunas (colunas) {}

Impresso::Impresso(){}

void Impresso::setLivrarias(std::vector<string> livrarias){
    this->livrarias = livrarias;
}

std::vector<string> Impresso::getLivrarias() const{
    return livrarias;
}

void Impresso::setColunas(int colunas){
    this->colunas = colunas;
}

int Impresso::getColunas() const{
    return colunas;
}

void Impresso::setCaracteristicaEspecifica(){}

void Impresso::print(std::ostream&out) const{
    Livro::print(out);
    out << livrarias.at(0);
}

std::ostream& operator<<(std::ostream& out, const Impresso& impresso ) {
    impresso.print(out);
    return out;   
} 