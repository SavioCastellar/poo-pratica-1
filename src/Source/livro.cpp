#include <iostream>
#include <string> 
#include <vector>
#include <list>

#include <string.h>
#include "../Headers/livro.h"

Livro::Livro(
    std::vector<string> escritoresValue, 
    std::vector<string> capitulos,
    std::vector<string> keywords,
    std::string titulo, 
    int anoPublicacao,
    std::string idiomaOriginal){
        setEscritores(escritoresValue);
        setCapitulos(capitulos);
        setKeywords(keywords);
        setTitulo(titulo);
        setAnoPublicacao(anoPublicacao);
        setIdiomaOriginal(idiomaOriginal);
    }

Livro::Livro(){}

void Livro::setEscritores(std::vector<string> escritores){
    this->escritores = escritores;
}

std::vector<string> Livro::getEscritores() const{
    return escritores;
}

void Livro::setTitulo(std::string tituloString){
   const char *tituloValue = tituloString.data();
    int length = tituloString.size();
    length = (length < 30 ? length : 29); 
    strncpy(titulo, tituloValue, length);
    for(int i=0; i<29-length;i++){
        titulo[i+length] = ' ';
    }
    titulo[29] = '\0';
}

std::string Livro::getTitulo() const{
    return titulo;
}

void Livro::setCapitulos(std::vector<string> capitulos){
    this->capitulos = capitulos;
}

std::vector<string> Livro::getCapitulos() const{
    return capitulos;
}

void Livro::setAnoPublicacao(int anoPublicacao){
    this->anoPublicacao = anoPublicacao;
}

int Livro::getAnoPublicacao() const{
    return anoPublicacao;
}

void Livro::setIdiomaOriginal(std::string idiomaOriginalString){
    const char *idiomaOriginalValue = idiomaOriginalString.data();
    int length = idiomaOriginalString.size();
    length = (length < 10 ? length : 9);
    strncpy(idiomaOriginal,idiomaOriginalValue,length);
    for(int i=0; i<9-length;i++){
        idiomaOriginal[i+length] = ' ';
    }
    idiomaOriginal[9] = '\0';
}

std::string Livro::getIdiomaOriginal() const{
    return idiomaOriginal;
}

void Livro::setKeywords(std::vector<string> keywords){
    this->keywords = keywords;
}

std::vector<string> Livro::getKeywords() const{
    return keywords;
}

void Livro::print(std::ostream& out) const{
    out << left << titulo << " | " 
        << fristEscritor << " | " 
        << idiomaOriginal << " | " ;
    if(capitulos.size() < 10) 
        out << "  " << capitulos.size() << " | ";
    else if(capitulos.size() < 100) 
        out << " " << capitulos.size() << " | ";
    else 
        out << " " << capitulos.size() << " | ";

    if(keywords.size() < 10)
        out  << " " << keywords.size() << " | ";
}

std::ostream& operator<<(std::ostream& out, const Livro& livro) {
    livro.print(out);
    return out;
} 

void Livro::setFirstEscritor(std::string escritorString){
    const char *escritorValue = escritorString.data();
    int length = escritorString.size();
    length = (length < 30 ? length : 29);
    strncpy(fristEscritor,escritorValue,length);
    for(int i=0; i<29-length;i++){
        fristEscritor[i+length] = ' ';
    }
    fristEscritor[29] = '\0';
}

std::string Livro::getFirstEscritor() const {
    return this->fristEscritor;
}
