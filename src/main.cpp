/*
Integrantes do grupo: 
    • Diego de Jesus Ferreira 
    • Mateus Jorge
    • Sávio Reis Castellar
    • Vitor Diocleciano
*/
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <filesystem>

#include "Headers/livro.h"
#include "Headers/audiobook.h"
#include "Headers/eletronico.h"
#include "Headers/impresso.h"

namespace fs = std::filesystem;


std::vector<string> lerArquivos(int argc, char *argv[]);
void split(std::string, std::vector<string>);
bool compararAno(Eletronico *, Eletronico *);
template <typename Type>
std::vector<Livro *> setLivroPorIdioma(std::vector<Livro *>, char, Type *);
std::vector<Livro *> selecionarLivroPorIdioma(std::vector<Livro *>, char);
bool BuscaAudiobookPorAutor(std::vector<Livro *> &, std::string);
std::vector<Eletronico *> BuscaEletronicoPorFormato(std::vector<Livro *> &, std::string);
template <typename Type2>
vector<Livro *> setLivrosPorTitulo(std::vector<Livro *>, char, Type2 *);
vector<Livro *> selecionarLivrosPorTitulo(vector<Livro *>, char);
template <typename Type>
int setLivroPorKeyword(std::vector<Livro*>, std::string, Type);
int retornarLivroPorKeyword(std::vector<Livro*>, std::string);
std::vector<Impresso*> retornarLivrosImpressos(std::vector<Livro*>, int);
void mapeiaIdioma(vector<Livro *>);
void exportaVetor(vector<string>);
std::vector<Livro *> criarLivros(std::vector<string>);
std::vector<Livro*> AutoresCap(std::vector<Livro*>&, int);
std::set<string> Oskeywords(std::vector<Livro*>&);
std::vector<Livro*> BuscaTitulo(std::vector<Livro*>&, std::string);


int main(int argc, char *argv[])
{
	std::vector<string> files_ = lerArquivos(argc, argv);
	std::vector<Livro *> Livros = criarLivros(files_);

	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra A:" << std::endl;
	// Teste de todos os livros
	for (auto i : Livros)
	{
		cout << *i << std::endl;
	} // fim teste de todos os livros
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra B:" << std::endl;
	const char idioma[10] = "Portugues";
	const std::vector<Livro *> livroPorIdioma = selecionarLivroPorIdioma(Livros, *idioma);
	for (auto livro : livroPorIdioma)
	{
		std::cout << *livro << std::endl;
	}
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra C:" << std::endl;
	// Teste do metodo letra C --> ordenaçao do maior para o menor
	std::vector<Eletronico *> filtroPorFormato = BuscaEletronicoPorFormato(Livros, "EPUB");
	sort(filtroPorFormato.begin(), filtroPorFormato.end(), compararAno);
	for (auto i : filtroPorFormato)
	{
		std::cout << *i << std::endl;
	} // fim teste do metodo letra C
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra D:" << std::endl;
	int num_livrarias = 4;
	const std::vector<Impresso*> livrosDisponiveisLivrarias = retornarLivrosImpressos(Livros, num_livrarias);
	for (auto livro : livrosDisponiveisLivrarias) {
		std::cout << *livro << std::endl;
	}
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra E:" << std::endl;
	// Teste do metodo Letra E
	if (BuscaAudiobookPorAutor(Livros, "J. K. Rowling"))
		std::cout << "Existe audiobook(s) com este autor." << std::endl;
	else
		std::cout << "Nao existe audiobook(s) com este autor." << std::endl;

	if (BuscaAudiobookPorAutor(Livros, "Faustao"))
		std::cout << "Existe audiobook(s) com este autor." << std::endl;
	else
		std::cout << "Nao existe audiobook(s) com este autor." << std::endl;

	if (BuscaAudiobookPorAutor(Livros, "Marcel Marlier"))
		std::cout << "Existe audiobook(s) com este autor." << std::endl;
	else
		std::cout << "Nao existe audiobook(s) com este autor." << std::endl;
	// Fim do teste metodo letra E

	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra F:" << std::endl;
	// metodo f
	std::vector<Livro*> testando= BuscaTitulo(Livros,"Harry Potter e a Pedra Filoso");
	for(auto i:testando){
		cout << *i << endl;
	}
	cout<<"\n";

	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra G:" << std::endl;
	//metodo g
	typedef std::set<string, std:: less<string>> stt;
	stt teste;
	teste=Oskeywords(Livros);
	int count = 0;
		for(auto i:teste){
			cout<< i<< " - ";
			if(count == 5) {
				cout << "\n";
				count = 0;
			} 
			count++;
			}
	cout<<"\n";

	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra H:" << std::endl;

	// metodo h
	int n =18;
	std::vector<Livro*> test;
	test= AutoresCap(Livros, n);
	for (auto i:test){
		cout<<"\n"<< *i<< " ";
		}
	cout<<"\n";

	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra I:" << std::endl;
	// Teste do metodo letra I
	const char titulo[30] = "Game of Thrones";
	const std::vector<Livro *> livroPorTitulo = selecionarLivrosPorTitulo(Livros, *titulo);
	for (auto livro : livroPorTitulo)
	{
		std::cout << *livro << std::endl;
	}
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra K:" << std::endl;
	const std::string keyword = "narrativa";
	const int quantidadeLivros = retornarLivroPorKeyword(Livros, keyword);
	std::cout << "Quantidade de livros encontrados dado a keyword " << keyword << ": " << quantidadeLivros << std::endl;
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;
	std::cout << "Questao letra L:" << std::endl;
	// Teste do metodo letra L
	mapeiaIdioma(Livros);
	std::cout << "-----------------------------------------------------------------------------------------------" << std::endl;

	exportaVetor(files_);
	return EXIT_SUCCESS;
}

/* Lê cada um dos  arquivos e armazena seu conteúdo em um vetor de strings.*/
std::vector<string> lerArquivos(int argc, char *argv[])
{
	const std::string folder = "../files/";
	std::vector<string> files_;
	const fs::path pathToShow{argc >= 2 ? argv[1] : fs::current_path()};
	for (const auto &entry : fs::directory_iterator(folder))
	{
		const auto filename = entry.path().filename().string();
		if (entry.is_regular_file())
		{
			files_.push_back(folder + filename);
		}
	}
	return files_;
}

void split(const std::string &str, std::vector<string> &cont, char delim = ' ')
{
	std::stringstream ss(str);
	std::string token;
	while (std::getline(ss, token, delim))
		cont.push_back(token);
}

/* 	Parâmetros:
		- e1: objeto da classe Eletronico;
		- e2: objeto da classe Eletronico;

	Executa o método que retorna o ano de publicação de cada um dos objetos
	e retorna verdadeiro se o primeiro for maior que o segundo.
*/
bool compararAno(Eletronico *e1, Eletronico *e2)
{
	return (e1->getAnoPublicacao() > e2->getAnoPublicacao());
}

template <typename Type>
std::vector<Livro*> setLivroPorIdioma(std::vector<Livro*> tituloLivros, char idioma, Type* ptr) {
	for (auto idioma_ : ptr->getIdiomaOriginal()) {
		if (idioma_ == idioma) {
			ptr->getTitulo();
			tituloLivros.push_back(ptr);
		}
	}
	return tituloLivros;
}

/* Seleciona livro por Idioma */
std::vector<Livro*> selecionarLivroPorIdioma(std::vector<Livro*> container, char idioma) {
	int iterator = 0;
	std::vector<Livro*> tituloLivros;
	for (auto index : container) {
		if (typeid(*index).name() == typeid(class Impresso).name()) {
			Impresso* ptr = dynamic_cast<Impresso*>(container.at(iterator));
			tituloLivros = setLivroPorIdioma(tituloLivros, idioma, ptr);
		}
		else if (typeid(*index).name() == typeid(class Eletronico).name()) {
			Eletronico* ptr = dynamic_cast<Eletronico*>(container.at(iterator));
			tituloLivros = setLivroPorIdioma(tituloLivros, idioma, ptr);
		}
		else if (typeid(*index).name() == typeid(class AudioBook).name()) {
			AudioBook* ptr = dynamic_cast<AudioBook*>(container.at(iterator));
			tituloLivros = setLivroPorIdioma(tituloLivros, idioma, ptr);
		}
		else continue;
		iterator++;
	}
	return tituloLivros;
}

bool BuscaAudiobookPorAutor(std::vector<Livro *> &livros, std::string autor)
{
	int count = 0;
	for (auto i : livros)
	{
		if (typeid(*i).name() == typeid(class AudioBook).name())
		{
			AudioBook *ptr = dynamic_cast<AudioBook *>(livros.at(count));
			for (auto j : ptr->getEscritores())
			{
				if (j == autor)
				{
					return true;
				}
			}
		}
		count++;
	}
	return false;
}

/* Dado um número de livrarias e se esse número for maior ou igual, retorna os titulos dos livros */
std::vector<Impresso*> retornarLivrosImpressos(std::vector<Livro*> container, int num_livrarias_) {
	int iterator = 0;
	std::vector<Impresso*> livrosEncontrados;
	int num_livrarias = 0;
	for (auto index : container) {
		if (typeid(*index).name() == typeid(class Impresso).name()) {
			Impresso* ptr = dynamic_cast<Impresso*>(container.at(iterator));
			for (auto livraria : ptr->getLivrarias()) {
				num_livrarias++;
				if (num_livrarias > num_livrarias_) {
					ptr->getTitulo();
					livrosEncontrados.push_back(ptr);
				}
			}
		}
		iterator++;
	}
	return livrosEncontrados;
}

std::vector<Eletronico *> BuscaEletronicoPorFormato(std::vector<Livro *> &livros, std::string formato)
{
	int count = 0;
	std::vector<Eletronico *> eletronico;
	for (auto i : livros)
	{
		if (typeid(*i).name() == typeid(class Eletronico).name())
		{
			Eletronico *ptr = dynamic_cast<Eletronico *>(livros.at(count));
			if (ptr->getFormatoArquivo() == formato)
			{
				eletronico.push_back(ptr);
			}
		}
		count++;
	}
	return eletronico;
}

template <typename Type>
int setLivroPorKeyword(std::vector<Livro*> container, std::string keyword, Type ptr) {
	int quantidade = 0;
	for(auto keyword_ : ptr->getKeywords()){
		if(keyword_ == keyword) {
			quantidade++;
		}
	}
	return quantidade;
}

/* Dado uma keyword, retorna a quantidade de livros encontrados */
int retornarLivroPorKeyword(std::vector<Livro*> container, std::string keyword) {
	int iterator = 0;
	int quantidade = 0;
	for(auto i : container){
		if(typeid(*i).name() ==  typeid(class Impresso).name()){
			Impresso* ptr = dynamic_cast<Impresso*>(container.at(iterator));
			quantidade = setLivroPorKeyword(container, keyword, ptr);
		}
		if(typeid(*i).name() ==  typeid(class Eletronico).name()){
			Eletronico* ptr = dynamic_cast<Eletronico*>(container.at(iterator));
			quantidade = setLivroPorKeyword(container, keyword, ptr);
		}
		if(typeid(*i).name() ==  typeid(class AudioBook).name()){
			AudioBook* ptr = dynamic_cast<AudioBook*>(container.at(iterator));
			quantidade = setLivroPorKeyword(container, keyword, ptr);
		}
		iterator++;
	}
	return quantidade;
}

template <typename Type2>
std::vector<Livro *> setLivrosPorTitulo(std::vector<Livro *> tituloLivros, char titulo, Type2 *ptr)
{
	for (auto titulo_ : ptr->getTitulo())
	{
		if (titulo_ == titulo)
		{
			ptr->getTitulo();
			tituloLivros.push_back(ptr);
		}
	}
	return tituloLivros;
}

std::vector<Livro *> selecionarLivrosPorTitulo(std::vector<Livro *> container, char titulo)
{
	int iterator = 0;
	std::vector<Livro *> tituloLivros;
	for (auto index : container)
	{	
		if (typeid(*index).name() == typeid(class Impresso).name())
		{
			Impresso *ptr = dynamic_cast<Impresso *>(container.at(iterator));
			tituloLivros = setLivrosPorTitulo(tituloLivros, titulo, ptr);
		}
		else if (typeid(*index).name() == typeid(class Eletronico).name())
		{
			Eletronico *ptr = dynamic_cast<Eletronico *>(container.at(iterator));
			tituloLivros = setLivrosPorTitulo(tituloLivros, titulo, ptr);
		}
		else if (typeid(*index).name() == typeid(class AudioBook).name())
		{
			AudioBook *ptr = dynamic_cast<AudioBook *>(container.at(iterator));
			tituloLivros = setLivrosPorTitulo(tituloLivros, titulo, ptr);
		}
		else
			continue;
		iterator++;
	}
	return tituloLivros;
}

void mapeiaIdioma(vector<Livro *> container){
	map<string, string> idiomas;
	// container.at(0)->getIdiomaOriginal
	for (auto i : container){
		if(i->getIdiomaOriginal()=="Ingles"){
			idiomas.insert(pair<string, string>("Ingles", "ING"));
		}
		else if(i->getIdiomaOriginal()=="Portugues"){
			idiomas.insert(pair<string, string>("Portugues", "POT"));
		}
		else if(i->getIdiomaOriginal()=="Espanhol"){
			idiomas.insert(pair<string, string>("Espanhol", "ESP"));
		}
		else if(i->getIdiomaOriginal()=="Frances"){
			idiomas.insert(pair<string, string>("Frances", "FRS"));
		}
	}
	for (auto t : idiomas){
		cout << t.first << " = " << t.second << "\n";}
	// cout << pair.first << " = " << pair.second << "}\n";
}

void exportaVetor(vector<string> files){
	std::string line;
		std::vector<string> saida;
		for (int book = 0; book <= (files.size() - 1); book++)
		{
			std::ifstream file(files[book]);
			int lineNumber = 1;
			while (std::getline(file, line))
			{
				if (line == "1" & lineNumber == 1)
				{
					saida.push_back("Impresso");
				}
				if (line == "2" & lineNumber == 1)
				{
					saida.push_back("Eletronico");
				}
				if (line == "3" & lineNumber == 1)
				{
					saida.push_back("AudioBook");
				}
			}
		}
	std::ofstream outFile("saida.txt");
	// the important part
	for (const auto e : saida) outFile << e << "\n";
}

//implementaçao do H
std::vector<Livro*> AutoresCap(std::vector<Livro*>& livros, int n){

	std::vector<Livro*> vec;
	//valido para 16 arquivos, se for adicionar criar um get
	for(int u=0; u<16; u++){
		if(livros.at(u)->getCapitulos().size()<n ){
			vec.push_back(livros.at(u));
		}
	}
	sort(vec.begin(),vec.end(), [](Livro * a, Livro* b){
		return a->getEscritores().at(0)<b->getEscritores().at(0);
	});
	return vec;
}

//implementaçao g
std::set<string> Oskeywords(std::vector<Livro*>& livros){
	typedef std::set<string, std:: less<string>> st;
	st setizinho;
		//valido para 16 arquivos, se for adicionar criar um get
		for(int u=0; u<16; u++){
			for(int i=0; i<livros.at(u)->getKeywords().size(); i++){
				setizinho.insert(livros.at(u)->getKeywords().at(i));
			}
		}
	return setizinho;
}

//implementação do F
std::vector<Livro*> BuscaTitulo(std::vector<Livro*>& livros, std::string titulo){
	int count = 0;
	vector<Livro*> busca;

	for(auto i: livros){
		if (livros.at(count)->getTitulo() == titulo){
			busca.push_back(livros.at(count));
			}
	count++;
	}	
	return busca;
}

	/* 	Parâmetro:
			- files: vetor contendo o nome dos arquivos a serem lidos;

		Executa a leitura dos arquivos e os armazena em vetores de ponteiros.
	*/
	std::vector<Livro *> criarLivros(std::vector<string> files)
	{
		std::string line;
		std::vector<Livro *> Livros;
		for (int book = 0; book <= (files.size() - 1); book++)
		{
			std::ifstream file(files[book]);
			int lineNumber = 1;
			while (std::getline(file, line))
			{
				if (line == "1" & lineNumber == 1)
				{
					Livros.push_back(new Impresso);
				}
				if (line == "2" & lineNumber == 1)
				{
					Livros.push_back(new Eletronico);
				}
				if (line == "3" & lineNumber == 1)
				{
					Livros.push_back(new AudioBook);
				}
				if (lineNumber == 2)
				{
					Livros.at(book)->setTitulo(line);
				}
				if (lineNumber == 3)
				{
					std::vector<string> cont;
					split(line, cont, ';');
					Livros.at(book)->setEscritores(cont);
					Livros.at(book)->setFirstEscritor(cont.at(0));
				}
				if (lineNumber == 4)
				{
					Livros.at(book)->setAnoPublicacao(stoi(line));
				}
				if (lineNumber == 5)
				{
					Livros.at(book)->setIdiomaOriginal(line);
				}
				if (lineNumber == 6)
				{
					std::vector<string> cont;
					split(line, cont, ';');
					Livros.at(book)->setKeywords(cont);
				}
				if (lineNumber == 7)
				{
					std::vector<string> cont;
					split(line, cont, ';');
					Livros.at(book)->setCapitulos(cont);
				}
				if (typeid(*Livros.at(book)).name() == typeid(class AudioBook).name())
				{
					AudioBook *ptr = dynamic_cast<AudioBook *>(Livros.at(book));
					if (lineNumber == 8)
					{
						ptr->setDuracao(stof(line));
					}
					if (lineNumber == 9)
					{
						ptr->setFormatoAudio(line);
					}
				}
				if (typeid(*Livros.at(book)).name() == typeid(class Impresso).name())
				{
					Impresso *ptr = dynamic_cast<Impresso *>(Livros.at(book));
					if (lineNumber == 8)
					{
						std::vector<string> cont;
						split(line, cont, ';');
						ptr->setLivrarias(cont);
					}
					if (lineNumber == 9)
					{
						ptr->setColunas(stoi(line));
					}
				}
				if (typeid(*Livros.at(book)).name() == typeid(class Eletronico).name())
				{
					Eletronico *ptr = dynamic_cast<Eletronico *>(Livros.at(book));
					if (lineNumber == 8)
					{
						ptr->setUrl(line);
					}
					if (lineNumber == 9)
					{
						ptr->setFormatoArquivo(line);
					}
				}
				lineNumber++;
			}
		}
		return Livros;
	}